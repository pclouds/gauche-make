(define-library (make)
  (import (scheme base) (scheme file)
          (srfi 1) (srfi 26) (srfi 69)
          (file util))
  (export make/proc *target* *depends*
          make-target
          *rules* add-rule! define-rule compile-rules)
  (include "make-lib.scm"))
