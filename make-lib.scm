(define *target* (make-parameter #f))
(define *depends* (make-parameter '()))

(define (flatten-targets spec)
  (append-map (lambda (x)
                (cond
                 [(string? (car x)) (list x)]
                 [(pair? (car x)) (map (lambda (y)
                                         (cons y (cdr x)))
                                       (car x))]
                 [else
                  (error "only accept pair or string" x)]))
              spec))

(define (remake? target deps)
  (call/cc
   (lambda (exit)
     (if (null? deps)
         (exit #t))
     (let ([target-mtime (or (file-mtime target) 0)])
       (for-each (lambda (x)
                   (if (< target-mtime (file-mtime x))
                       (exit #t)))
                 deps)
       (exit #f)))))

(define (compile-rules spec)
  (alist->hash-table (flatten-targets spec) string=?))

(define (make-target ht target)
  (if (hash-table-exists? ht target)
      (let ([rule (hash-table-ref ht target)])
        (for-each (lambda (x)
                    (make-target ht x))
                  (car rule))
        (if (and (pair? (cdr rule))
                 (remake? target (car rule)))
            (parameterize ([*target* target]
                           [*depends* (car rule)])
              ((cadr rule)))))
      (unless (file-exists? target)
        (error "target not found" target))))

(define (make/proc spec args)
  (let ([ht (compile-rules spec)])
    (if (string? args)
        (make-target ht args)
        (for-each (cut make-target ht <>) args))))

(define *rules* (make-parameter '()))

(define (add-rule! target . deps)
  (set! (*rules*) (cons (cons target deps) (*rules*))))

(define-syntax define-rule
  (syntax-rules ()
    ((_ target deps expr) (add-rule! 'target 'deps (lambda () expr)))
    ((_ target deps) (add-rule! 'target 'deps))))
